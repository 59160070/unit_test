/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unit_test;

public class Player {

    private char name;

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public String toString() {
        return "Player{" +"name="+ name +"}";
        
    }
}
