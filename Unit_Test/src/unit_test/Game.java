package unit_test;

import java.util.Scanner;

public class Game {

    private Scanner kb = new Scanner(System.in);
    private int row;
    private int col;
    Table table = null;
    Player o = null;
    Player x = null;

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }

    public void run() {
        this.showWelcome();
        this.table = new Table(o, x);
        while (true) {
            this.showTable();
            this.showTurn();
            this.inputRowCol();

            if (table.checkWin()) {
                return;
            }
            table.switchPlayer();
        }
    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] data = this.table.getData();
        for (int row = 0; row < data.length; row++) {
            System.out.print("|");
            for (int col = 0; col < data[row].length; col++) {
                System.out.print(" " + data[row][col] + " |");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " Turn");
    }

    private void input() {
        System.out.print("Please input row,col: ");
        row = kb.nextInt();
        col = kb.nextInt();

    }

    private void inputRowCol() {
        while (true) {
            this.input();
            if (table.setRowCol(row, col)) {
                return;
            }
        }
    }
}
